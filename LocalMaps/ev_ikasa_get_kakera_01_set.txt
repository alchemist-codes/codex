{"party":[{"x":6,"y":8,"dir":0,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0},{"x":6,"y":9,"dir":0,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0},{"x":5,"y":6,"dir":3,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0},{"x":6,"y":6,"dir":3,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0},{"x":4,"y":6,"dir":3,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0}],"enemy":[{"x":11,"y":3,"dir":1,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":5,"search":10,"abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]},{"x":5,"y":2,"dir":1,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":5,"search":10,"abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]},{"x":12,"y":11,"dir":3,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":3,"search":10,"abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]},{"x":11,"y":10,"dir":3,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":3,"search":10,"abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]},{"x":6,"y":1,"dir":1,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":6,"search":10,"abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]},{"x":12,"y":4,"dir":1,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":6,"search":10,"abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]},{"x":4,"y":9,"dir":0,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":4,"search":10,"abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]},{"x":4,"y":8,"dir":0,"ai_type":0,"ai_x":0,"ai_y":0,"ai_len":0,"ai":"AI_ENEMY","iname":"UN_V2_EN_MAFIA_KIL_ALSO_SPRACH","side":1,"lv":75,"rare":4,"awake":15,"elem":4,"search":10,"drop":"QE_EV_IKASA_GET_KAKERA_KOTEI","abils":[{"iname":"AB_KIL_JLV11","rank":1},{"iname":"AB_KIL_UPPER","rank":20,"skills":[{"iname":"SK_KIL_01","rate":0,"check_cnt":0},{"iname":"SK_KIL_02","rate":100,"check_cnt":0},{"iname":"SK_KIL_03","rate":100,"check_cnt":0},{"iname":"SK_KIL_04","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_LOWER","rank":20,"skills":[{"iname":"SK_KIL_05","rate":100,"check_cnt":0},{"iname":"SK_KIL_06","rate":0,"check_cnt":0},{"iname":"SK_KIL_07","rate":0,"check_cnt":0},{"iname":"SK_KIL_08","rate":100,"check_cnt":0},{"iname":"SK_KIL_09","rate":0,"check_cnt":0}]},{"iname":"AB_KIL_REACTION_01","rank":10},{"iname":"AB_KIL_SUPPORT_01","rank":1},{"iname":"AB_LS_100-99900_U_HP","rank":5},{"iname":"AB_LS_1-999_U_ATK","rank":20},{"iname":"AB_DOWN_RENKEI_1_2","rank":2}]}],"w_cond":{"actions":[],"goals":[],"withdraw":[]},"l_cond":{"actions":[],"goals":[],"withdraw":[]}}