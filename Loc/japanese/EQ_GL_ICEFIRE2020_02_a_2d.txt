﻿001	God, just how far is this place?
002	...soon!
003	Chill out, getting more power just for a hike? Now that's great value for effort!
004	Thanks Eira, for bringing us to meet your sister.
005	The boss may be a tad... eccentric, but we do share the dream of protecting the people.
006	protect... people! Good!
007	And maybe becoming heroes, like the ones always mentioned in myths!
008	Not just heroes, our feats will be so grand, it will literally become the beacon of hope for thousands of upcoming adventurers!
009	Our mark on the world will forever be engraved in history!
010	Ooo...
011	Perfect, a little warmup. Watch closely chump, this is why we're considered strong even amongst adventurers!
012	...chump?
013	Just ignore him Eira. We might need your support as well. Don't be afraid to help out!
014	Ok!